function strfn()
{
    var a : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var b : string = a.value;
    var c : string;

    document.getElementById("display").innerHTML += "<b>The original String is : </b>" + b;

    c = b.substring(4,10);
    document.getElementById("display").innerHTML += "</br><b>b.substring(4,10) : </b>" + c;

    c = b.charAt(3);
    document.getElementById("display").innerHTML += "</br><b>b.charAt(3) : </b>" + c;
}