

function check1()
{
    var positive_count: number = 0;
    var negative_count: number = 0;
    var zero_count: number = 0;

    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let positive:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("positive");
    let negative: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("negative");
    let zero: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("zero");

    var numb: number = parseInt(t1.value); //to get no. of numbers
    
   for(var i=0;i<numb;i++)
   {
       var c = (prompt("Enter a number"));//it is in string format
       var d:number = parseFloat(c);// it is converted into float
       if(d>0)
       {
           positive_count++; 
       }
       else if(d<0)
       {
           negative_count++;
       }
       else
       {
           zero_count++;
       }
   }  
    console.log("pos" + " " + positive_count);
    console.log("neg" + " " + negative_count);
    console.log("zer" + " " + zero_count);

    //to put no. of pos , neg , zero numbers in paragraph tag
    positive.innerHTML = positive_count.toString(); 
    negative.innerHTML = negative_count.toString();
    zero.innerHTML = zero_count.toString();
}
