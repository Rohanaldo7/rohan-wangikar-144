function check1() {
    var positive_count = 0;
    var negative_count = 0;
    var zero_count = 0;
    let t1 = document.getElementById("t1");
    let positive = document.getElementById("positive");
    let negative = document.getElementById("negative");
    let zero = document.getElementById("zero");
    var numb = parseInt(t1.value); //to get no. of numbers
    for (var i = 0; i < numb; i++) {
        var c = (prompt("Enter a number")); //it is in string format
        var d = parseFloat(c); // it is converted into float
        if (d > 0) {
            positive_count++;
        }
        else if (d < 0) {
            negative_count++;
        }
        else {
            zero_count++;
        }
    }
    console.log("pos" + " " + positive_count);
    console.log("neg" + " " + negative_count);
    console.log("zer" + " " + zero_count);
    //to put no. of pos , neg , zero numbers in paragraph tag
    positive.innerHTML = positive_count.toString();
    negative.innerHTML = negative_count.toString();
    zero.innerHTML = zero_count.toString();
}
//# sourceMappingURL=signofnum.js.map